/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapeproject;

/**
 *
 * @author Acer
 */
public class Rectangle {
    private double w;
    private double l;
    public Rectangle(double w,double l){
        this.w=w;
        this.l=l;
    }public double calArea(){
        return w*l;
    }public double getW(){
        return this.w;
    }public double getL(){
        return this.l;
    }public void setWL(double w,double l){
        if(w>l || w==l || l==0 || w==0 ){
            System.out.println("Error it impossible");
            return;
        }
        this.w=w;
        this.l=l;
    }
    public String toString(){
        return "Area of Rectangle (w = "+ this.getW() + " and l = "+ this.getL() + ") is " + this.calArea();
    }
}
