/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapeproject;

/**
 *
 * @author Acer
 */
public class Triangle {
    private double b;
    private double h;
    static final double s = 2.0;
    public Triangle(double b, double h){
    this.h=h;
    this.b=b;
    }public double calArea(){
        return b*h/s;
    }public double getB(){
        return this.b;
    }public double getH(){
        return this.h;
    }public void setBH(double b, double h){
        if(b ==0 || h == 0){
            System.out.println("Error it impossible");
            return;
        }this.b=b;
        this.h=h;
    }
    public String toString(){
        return "Area of Triangle (b = "+ this.getB() + " and h = "+ this.getH() + " and / 2 " + ") is " + this.calArea();
    }
}
