/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapeproject;

/**
 *
 * @author Acer
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4,12);
        System.out.println("Area of Rectangle (w = "+ rectangle1.getW() + " and l = "+ rectangle1.getL() + ") is " + rectangle1.calArea());
        rectangle1.setWL(5,5);
        System.out.println("Area of Rectangle (w = "+ rectangle1.getW() + " and l = "+ rectangle1.getL() + ") is " + rectangle1.calArea());
        rectangle1.setWL(6,3);
        System.out.println("Area of Rectangle (w = "+ rectangle1.getW() + " and l = "+ rectangle1.getL() + ") is " + rectangle1.calArea());
        rectangle1.setWL(0,0);
        System.out.println("Area of Rectangle (w = "+ rectangle1.getW() + " and l = "+ rectangle1.getL() + ") is " + rectangle1.calArea());
        System.out.println(rectangle1);
    }
}
